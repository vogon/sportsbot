setup
-----
1. run `npm i`.
2. run `tsc`.
3. run `node out/make-google-token.js` and follow the instructions.
4. create a `.env` file following the example in `dotenv.example`.
5. run `node out/main.js`.
