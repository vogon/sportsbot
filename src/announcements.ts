const ANNOUNCMENT_STRINGS = [
    "are u ready for some {0}",
    "llllllllllllllllllllllet's get ready to {0}",
    "llllllllllllllet's get ready to {0}",
    "lllllllet's get ready to {0}",
    "let's get ready to {0}",
    "sorry mom, but {0} is my life now",
    "sorry mom, but i'm risking it all to play {0}",
    "just got a {0} scholarship",
    "we're going to make it to the {0} nationals!!!!",
    "get in losers we're playing {0}",
    "im watching a jon bois video about {0}",
    "YES im a robot, YES i play {0}",
    "I am and will always be just simply a {0e} player,my tomb stone will just say. {0}.",
    "{0}: it's what's for dinner",
    "time for some {0}",
    "actually it's called {0}ball",
    "{0}...is my business",
    "{0} is not a crime",
]

// not using these yet but i made the list manually so im leaving it here
const SPORTS_EMOJIS = [
    "🏄‍♀️",
    "🤽‍♀️",
    "🚴‍♀️",
    "⛹️‍♀️",
    "🚣‍♀️",
    "🤾‍♀️",
    "🏊‍♀️",
    "🚣‍♂️",
    "🏒",
    "🏓",
    "🏑",
    "🏐",
    "🏏",
    "🛶",
    "🏸",
    "🏹",
    "🏎",
    "🏉",
    "🏈",
    "🏇",
    "🏅",
    "⚾️",
    "🏂",
    "🏁",
    "🏀",
    "⛳️",
    "🤺",
    "🥊",
    "🥅",
    "🥍",
    "🥎",
    "🎾",
    "🎳",
    "⚽️",
]

export function getAnnouncement(sport: string): string {
    const index = Math.floor(Math.random() * ANNOUNCMENT_STRINGS.length);

    const out = ANNOUNCMENT_STRINGS[index]
        .replace("{0}", sport)
        // this is for a single joke
        .replace("{0e}", sport.replace("e", ""))

    return out
}
