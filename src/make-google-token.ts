import * as fs from "fs";
import { google } from "googleapis";
import * as readline from "readline";

const SCOPES = [ "https://www.googleapis.com/auth/calendar.readonly" ];
const TOKEN_PATH = ".google/token.json";

fs.readFile(".google/credentials.json", (err, content) => {
    if (err) {
        console.error(`error loading client secret file: ${err}`);
        process.exit(255);
    }

    getAccessToken(JSON.parse(content.toString()));
});

function getAccessToken(credentials) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(client_id, client_secret,
        redirect_uris[0]);
        
    const authUrl = oAuth2Client.generateAuthUrl({
        access_type: "offline", scope: SCOPES
    });

    console.log(`authorize this app by visiting: ${authUrl}`);
    const rl = readline.createInterface({
        input: process.stdin, output: process.stdout
    });
    
    rl.question("enter the code from that page here:", (code) => {
        rl.close();
        oAuth2Client.getToken(code, (err, token) => {
            if (err) return console.error(`error retrieving access token: ${err}`);

            oAuth2Client.setCredentials(token);
            fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                if (err) return console.error(err);
                console.log(`token stored to ${TOKEN_PATH}`);
            });
        });
    });
}
