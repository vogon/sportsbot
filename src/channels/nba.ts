import * as _ from "lodash";
import { Team, TeamGame } from "../team-game";
import { GcalHandler } from "../gcal-handler";

const ALL_TEAMS: Team[] = [
    {
        gcalId: "nba_1_%41tlanta+%48awks#sports@group.v.calendar.google.com",
        id: "ATL", location: "Atlanta", team: "Hawks"
    },
    {
        gcalId: "nba_2_%42oston+%43eltics#sports@group.v.calendar.google.com",
        id: "BOS", location: "Boston", team: "Celtics"
    },
    {
        gcalId: "nba_17_%42rooklyn+%4eets#sports@group.v.calendar.google.com",
        id: "BKN", location: "Brooklyn", team: "Nets"
    },
    {
        gcalId: "nba_30_%43harlotte+%48ornets#sports@group.v.calendar.google.com",
        id: "CHA", location: "Charlotte", team: "Hornets"
    },
    {
        gcalId: "nba_4_%43hicago+%42ulls#sports@group.v.calendar.google.com",
        id: "CHI", location: "Chicago", team: "Bulls"
    },
    {
        gcalId: "nba_5_%43leveland+%43avaliers#sports@group.v.calendar.google.com",
        id: "CLE", location: "Cleveland", team: "Cavaliers"
    },
    {
        gcalId: "nba_6_%44allas+%4davericks#sports@group.v.calendar.google.com",
        id: "DAL", location: "Dallas", team: "Mavericks"
    },
    {
        gcalId: "nba_7_%44enver+%4euggets#sports@group.v.calendar.google.com",
        id: "DEN", location: "Denver", team: "Nuggets"
    },
    {
        gcalId: "nba_8_%44etroit+%50istons#sports@group.v.calendar.google.com",
        id: "DET", location: "Detroit", team: "Pistons"
    },
    {
        gcalId: "nba_9_%47olden+%53tate+%57arriors#sports@group.v.calendar.google.com",
        id: "GSW", location: "Golden State", team: "Warriors"
    },
    {
        gcalId: "nba_10_%48ouston+%52ockets#sports@group.v.calendar.google.com",
        id: "HOU", location: "Houston", team: "Rockets"
    },
    {
        gcalId: "nba_11_%49ndiana+%50acers#sports@group.v.calendar.google.com",
        id: "IND", location: "Indiana", team: "Pacers"
    },
    {
        gcalId: "nba_12_%4cos+%41ngeles+%43lippers#sports@group.v.calendar.google.com",
        id: "LAC", location: "Los Angeles", team: "Clippers"
    },
    {
        gcalId: "nba_13_%4cos+%41ngeles+%4cakers#sports@group.v.calendar.google.com",
        id: "LAL", location: "Los Angeles", team: "Lakers"
    },
    {
        gcalId: "nba_29_%4demphis+%47rizzlies#sports@group.v.calendar.google.com",
        id: "MEM", location: "Memphis", team: "Grizzlies"
    },
    {
        gcalId: "nba_14_%4diami+%48eat#sports@group.v.calendar.google.com",
        id: "MIA", location: "Miami", team: "Heat"
    },
    {
        gcalId: "nba_15_%4dilwaukee+%42ucks#sports@group.v.calendar.google.com",
        id: "MIL", location: "Milwaukee", team: "Bucks"
    },
    {
        gcalId: "nba_16_%4dinnesota+%54imberwolves#sports@group.v.calendar.google.com",
        id: "MIN", location: "Minnesota", team: "Timberwolves"
    },
    {
        gcalId: "nba_3_%4eew+%4frleans+%50elicans#sports@group.v.calendar.google.com",
        id: "NOP", location: "New Orleans", team: "Pelicans"
    },
    {
        gcalId: "nba_18_%4eew+%59ork+%4bnicks#sports@group.v.calendar.google.com",
        id: "NYK", location: "New York", team: "Knicks"
    },
    {
        gcalId: "nba_25_%4fklahoma+%43ity+%54hunder#sports@group.v.calendar.google.com",
        id: "OKC", location: "Oklahoma City", team: "Thunder"
    },
    {
        gcalId: "nba_19_%4frlando+%4dagic#sports@group.v.calendar.google.com",
        id: "ORL", location: "Orlando", team: "Magic"
    },
    {
        gcalId: "nba_20_%50hiladelphia+76ers#sports@group.v.calendar.google.com",
        id: "PHI", location: "Philadelphia", team: "76ers"
    },
    {
        gcalId: "nba_21_%50hoenix+%53uns#sports@group.v.calendar.google.com",
        id: "PHX", location: "Phoenix", team: "Suns"
    },
    {
        gcalId: "nba_22_%50ortland+%54rail+%42lazers#sports@group.v.calendar.google.com",
        id: "POR", location: "Portland", team: "Trail Blazers"
    },
    {
        gcalId: "nba_23_%53acramento+%4bings#sports@group.v.calendar.google.com",
        id: "SAC", location: "Sacramento", team: "Kings"
    },
    {
        gcalId: "nba_24_%53an+%41ntonio+%53purs#sports@group.v.calendar.google.com",
        id: "SAS", location: "San Antonio", team: "Spurs"
    },
    {
        gcalId: "nba_31_%54eam+%47iannis+%41ll-%53tars#sports@group.v.calendar.google.com",
        id: "GIA", location: "Team Giannis", team: "All-Stars"
    },
    {
        gcalId: "nba_32_%54eam+%4ce%42ron+%41ll-%53tars#sports@group.v.calendar.google.com",
        id: "LEB", location: "Team LeBron", team: "All-Stars"
    },
    {
        gcalId: "nba_28_%54oronto+%52aptors#sports@group.v.calendar.google.com",
        id: "TOR", location: "Toronto", team: "Raptors"
    },
    {
        gcalId: "nba_26_%55tah+%4aazz#sports@group.v.calendar.google.com",
        id: "UTA", location: "Utah", team: "Jazz"
    },
    {
        gcalId: "nba_27_%57ashington+%57izards#sports@group.v.calendar.google.com",
        id: "WAS", location: "Washington", team: "Wizards"
    }
];

class NbaGame extends TeamGame {
    protected static teamsByTeamName =
        new Map(_.map(ALL_TEAMS, t => [t.team, t]));
}

let basketball = new GcalHandler<NbaGame>(
    NbaGame, _.map(ALL_TEAMS, t => t.gcalId), "hoops"
);

export default basketball;