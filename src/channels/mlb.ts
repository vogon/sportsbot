import * as _ from "lodash";
import { Team, TeamGame } from "../team-game";
import { GcalHandler } from "../gcal-handler";

const ALL_TEAMS: Team[] = [
    {
        gcalId: "mlb_1_%42altimore+%4frioles#sports@group.v.calendar.google.com",
        id: "BAL", location: "Baltimore", team: "Orioles"
    },
    {
        gcalId: "mlb_2_%42oston+%52ed+%53ox#sports@group.v.calendar.google.com",
        id: "BOS", location: "Boston", team: "Red Sox"
    },
    {
        gcalId: "mlb_3_%4cos+%41ngeles+%41ngels#sports@group.v.calendar.google.com",
        id: "LAA", location: "Los Angeles", team: "Angels"
    },
    {
        gcalId: "mlb_4_%43hicago+%57hite+%53ox#sports@group.v.calendar.google.com",
        id: "CWS", location: "Chicago", team: "White Sox"
    },
    {
        gcalId: "mlb_5_%43leveland+%49ndians#sports@group.v.calendar.google.com",
        id: "CLE", location: "Cleveland", team: "Indians"
    },
    {
        gcalId: "mlb_6_%44etroit+%54igers#sports@group.v.calendar.google.com",
        id: "DET", location: "Detroit", team: "Tigers"
    },
    {
        gcalId: "mlb_7_%4bansas+%43ity+%52oyals#sports@group.v.calendar.google.com",
        id: "KC", location: "Kansas City", team: "Royals"
    },
    {
        gcalId: "mlb_8_%4dilwaukee+%42rewers#sports@group.v.calendar.google.com",
        id: "MIL", location: "Milwaukee", team: "Brewers"
    },
    {
        gcalId: "mlb_9_%4dinnesota+%54wins#sports@group.v.calendar.google.com",
        id: "MIN", location: "Minnesota", team: "Twins"
    },
    {
        gcalId: "mlb_10_%4eew+%59ork+%59ankees#sports@group.v.calendar.google.com",
        id: "NYY", location: "New York", team: "Yankees"
    },
    {
        gcalId: "mlb_11_%4fakland+%41thletics#sports@group.v.calendar.google.com",
        id: "OAK", location: "Oakland", team: "Athletics"
    },
    {
        gcalId: "mlb_12_%53eattle+%4dariners#sports@group.v.calendar.google.com",
        id: "SEA", location: "Seattle", team: "Mariners"
    },
    {
        gcalId: "mlb_13_%54exas+%52angers#sports@group.v.calendar.google.com",
        id: "TEX", location: "Texas", team: "Rangers"
    },
    {
        gcalId: "mlb_14_%54oronto+%42lue+%4aays#sports@group.v.calendar.google.com",
        id: "TOR", location: "Toronto", team: "Blue Jays"
    },
    {
        gcalId: "mlb_15_%41tlanta+%42raves#sports@group.v.calendar.google.com",
        id: "ATL", location: "Atlanta", team: "Braves"
    },
    {
        gcalId: "mlb_16_%43hicago+%43ubs#sports@group.v.calendar.google.com",
        id: "CHC", location: "Chicago", team: "Cubs"
    },
    {
        gcalId: "mlb_17_%43incinnati+%52eds#sports@group.v.calendar.google.com",
        id: "CIN", location: "Cincinnati", team: "Reds"
    },
    {
        gcalId: "mlb_18_%48ouston+%41stros#sports@group.v.calendar.google.com",
        id: "HOU", location: "Houston", team: "Astros"
    },
    {
        gcalId: "mlb_19_%4cos+%41ngeles+%44odgers#sports@group.v.calendar.google.com",
        id: "LAD", location: "Los Angeles", team: "Dodgers"
    },
    {
        gcalId: "mlb_20_%57ashington+%4eationals#sports@group.v.calendar.google.com",
        id: "WSH", location: "Washington", team: "Nationals"
    },
    {
        gcalId: "mlb_21_%4eew+%59ork+%4dets#sports@group.v.calendar.google.com",
        id: "NYM", location: "New York", team: "Mets"
    },
    {
        gcalId: "mlb_22_%50hiladelphia+%50hillies#sports@group.v.calendar.google.com",
        id: "PHI", location: "Philadelphia", team: "Phillies"
    },
    {
        gcalId: "mlb_23_%50ittsburgh+%50irates#sports@group.v.calendar.google.com",
        id: "PIT", location: "Pittsburgh", team: "Pirates"
    },
    {
        gcalId: "mlb_24_%53t.+%4couis+%43ardinals#sports@group.v.calendar.google.com",
        id: "STL", location: "St. Louis", team: "Cardinals"
    },
    {
        gcalId: "mlb_25_%53an+%44iego+%50adres#sports@group.v.calendar.google.com",
        id: "SD", location: "San Diego", team: "Padres"
    },
    {
        gcalId: "mlb_26_%53an+%46rancisco+%47iants#sports@group.v.calendar.google.com",
        id: "SF", location: "San Francisco", team: "Giants"
    },
    {
        gcalId: "mlb_27_%43olorado+%52ockies#sports@group.v.calendar.google.com",
        id: "COL", location: "Colorado", team: "Rockies"
    },
    {
        gcalId: "mlb_28_%4diami+%4darlins#sports@group.v.calendar.google.com",
        id: "MIA", location: "Miami", team: "Marlins"
    },
    {
        gcalId: "mlb_29_%41rizona+%44iamondbacks#sports@group.v.calendar.google.com",
        id: "ARI", location: "Arizona", team: "Diamondbacks"
    },
    {
        gcalId: "mlb_30_%54ampa+%42ay+%52ays#sports@group.v.calendar.google.com",
        id: "TB", location: "Tampa Bay", team: "Rays"
    },
    {
        gcalId: "mlb_31_%41%4c+%41ll-%53tars#sports@group.v.calendar.google.com",
        id: "AL", location: "AL", team: "All-Stars"
    },
    {
        gcalId: "mlb_32_%4e%4c+%41ll-%53tars#sports@group.v.calendar.google.com",
        id: "NL", location: "NL", team: "All-Stars"
    }
];

class MlbGame extends TeamGame {
    protected static teamsByTeamName =
        new Map(_.map(ALL_TEAMS, t => [t.team, t]));
}

let baseball = new GcalHandler<MlbGame>(
    MlbGame, _.map(ALL_TEAMS, t => t.gcalId), "baseball"
);

export default baseball;