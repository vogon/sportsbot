import * as Discord from "discord.js";
import { calendar_v3 } from "googleapis";
import * as _ from "lodash";
import * as moment from "moment";
import { ChannelHandler, EventBucketEntry, Game as BaseGame } from "./channel-handler";

export class GcalHandler<T extends BaseGame> implements ChannelHandler<T> {
    constructor(private ctor: new (event: calendar_v3.Schema$Event) => T,
        public calendarIds: string[], public roleName: string) {}

    public gamesForEventBucket(entries: EventBucketEntry[]): T[] {
        // prune out duplicate copies of the same game on different calendars
        let eventGroups: {
            canonicalEvent: calendar_v3.Schema$Event,
            calendarIds: string[]
        }[] = [];

        for (let entry of entries) {
            let newGame = true;

            for (let group of eventGroups) {
                if (group.canonicalEvent.summary === entry.event.summary) {
                    // already have a copy of this game; add this calendar to the
                    // id list
                    group.calendarIds.push(entry.calendarId);
                    newGame = false;
                }
            }

            if (newGame) {
                eventGroups.push({
                    canonicalEvent: entry.event,
                    calendarIds: [entry.calendarId]
                });
            }
        }

        // build game objects
        let games: T[] = [];

        for (let group of eventGroups) {
            games.push(new this.ctor(group.canonicalEvent));
        }

        return games;
    }
    
    public announceGames(channel: Discord.TextChannel, games: T[],
            warningMinutes: number, debug: boolean, pingRole: boolean) {
        let msg = "";

        // appropriate Sports Role
        if (pingRole) {
            let roleId = channel.guild.roles.find("name", this.roleName).id;
    
            if (debug) {
                msg += `[would mention role ${roleId} (${this.roleName})] `;
            } else {
                msg += `<@&${roleId}> `;
            }
        }

        // number of games starting
        msg += (games.length === 1) ? "a game is" :
            `${games.length} games are`;
    
        // when they're starting
        msg += ` starting in ${warningMinutes} minutes ` +
            `(at ${moment(games[0].startTime).format("H:mm")} ` +
            `GMT${moment(games[0].startTime).format("Z")}):\n`;
    
        // which games are they
        msg += _.map(games, g => {
            // a game is starting in 15 minutes (at 16:00 GMT-08:00): Charlotte
            // Hornets at Indiana Pacers
            g.announced = true;
            
            return `- ${g.toString()}`;
        }).join("\n");
    
        channel.send(msg);
    }
}