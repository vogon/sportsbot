import { calendar_v3 } from "googleapis";
import * as _ from "lodash";
import * as moment from "moment";
import { Game as BaseGame } from "./channel-handler";

export interface Team {
    id: string;
    location: string;
    team: string;
    gcalId: string;
}

// todo (vogon): this code isn't capable of properly handling the all-star game
// since google lists it as an event with the name "All-Stars @ All-Stars",
// which is useless for figuring out who the home team is
export class TeamGame implements BaseGame {
    constructor(event: calendar_v3.Schema$Event) {
        const { awayTeam, homeTeam } = this.inferTeams(event.summary);

        this.startTime = moment(event.start.dateTime).toDate();
        this.announced = false;
        this.awayTeam = awayTeam;
        this.homeTeam = homeTeam;
    }

    protected static teamsByTeamName: Map<string, Team>;

    private inferTeams(eventSummary: string): { awayTeam: Team, homeTeam: Team } {
        // this regex needs to match both strings formatted like
        // "Trail Blazers @ Devil Rays", and strings formatted like
        // "Trail Blazers (69) @ Devil Rays (420)"
        let teamExtractor = /^([^\(@]+)\s*(?:\(\d+\))?\s*@\s*([^\(@]+)\s*(?:\(\d+\))?$/;

        let [, beforeAt, afterAt] = eventSummary.match(teamExtractor);
        let result = { awayTeam: undefined, homeTeam: undefined };

        beforeAt = beforeAt.trim();
        afterAt = afterAt.trim();

        // hack to allow this method to reach into subclasses' static fields 
        // courtesy of
        // https://github.com/microsoft/TypeScript/issues/3841#issuecomment-578475820
        let teamsByTeamName = this.constructor["teamsByTeamName"];

        result.awayTeam = teamsByTeamName.get(beforeAt) ||
            console.warn(`inferTeams: couldn't find away team '${beforeAt}' for event '${eventSummary}'`);
        result.homeTeam = teamsByTeamName.get(afterAt) ||
            console.warn(`inferTeams: couldn't find home team '${afterAt}' for event '${eventSummary}'`);

        return result;
    }

    public toString(): string {
        return `${this.awayTeam.location} ${this.awayTeam.team} at ` +
            `${this.homeTeam.location} ${this.homeTeam.team}`;
    }

    startTime: Date;
    announced: boolean;
    awayTeam: Team;
    homeTeam: Team;

    get pk(): string {
        return `${this.startTime.getTime()}:${this.awayTeam.id}:${this.homeTeam.id}`;
    }
}